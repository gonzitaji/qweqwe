# Servidor interno Ing&Dev

## Setup

```bash
    git clone https://gonzitaji@bitbucket.org/gonzitaji/servidor-node-ingydev.git

    cd servidor-node-ingydev

    npm install
```

## Inicialización

```bash
npm run start
# ejecuta: node /bin/www
```

Aún cuando se sabe que el script de entrada es /bin/www, utilice `npm run start`, o `npm start`. En el futuro la parametrización del comando puede cambiar (además, es buena práctica ejecutar todos de la misma manera ;) ).

## Config

### Variables de entorno

El sistema utiliza las siguientes variables de entorno:

variable | valor por defecto | tipos
--|--|--|--
NODE_ENV | produccion | `produccion` ó `desarrollo`
PORT | 3000 | any: se asume puerto si es numerico, y pipa si no

La configuración de las variables se hace en un archivo `.env` en la raíz del repositorio.

La validación de las variables se realiza en `/bin/www`. Cualquier variable inválida queda asignada a su valor por defecto, los cuales esán definidos directamente en el código.

* Para CAMBIAR estas variables:

    1. Crear archivo `.env` en la raíz del repositorio, si no lo ha hecho.
    2. Cambiar según las necesidades de su ambiente.

* Para AGREGAR una variable

    1. Agregar variable a su archivo `.env`.
    2. Agregar manejo de caso por defecto en `/bin/www`.
    3. Actualizar la tabla anterior y manejar caso por defecto.

### Config TODO:

1. En vez de usar variables de entorno por defecto en duro, utilizar un script como p.e.: /bin/setup, que cree un .env con las variables por defecto, al que se llame por npm scripts

2. Modulo que contenga y sirva todas las variables *relacionadas* con las variables de entorno, como ruta de conexiones, nombres de bd, etc. Dejar para cuando sea necesario gestionar más cómodamente las variables de entoreno