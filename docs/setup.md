# Levantamiento de la aplicación

## I. Preparar ambiente de desarrollo

### 1. Agregar repositorio de node como fuente 

```
cd ~
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
```

### 2. Instalar Nodejs

`sudo apt-get install nodejs`

## II. Preparar aplicación de node

### 1. Crear aplicación que levante un servidor de node.

`???`

### 2. Configurar node como el ambiente de la app en el script de entrada

index.js (primera línea) 

`#!/usr/bin/env nodejs`

### 3. Agregar privilegios de ejecución

```bash
chmod +x ./index.js
# probar con:
./index.js
```

## III. Levantar aplicación como demonio

### 1. Instalar PM2

`sudo npm install -g pm2`

### 2. Levantar app como un proceso

`pm2 start index.js`

### 3. configurar pm2 para correr cuando inicia el SO

```bash
pm2 startup systemd
# Correr comando que pm2 muestre en la consola
pm2 save
```

## IV. Levantar demonio como servicio web

### 1. Habilitar paquetes necesarios

```bash
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests
```

### 2. Agregar virtualhost en sites-available

```bash
# /etc/apache2/sites-available/mi-sub-dominio.conf
<VirtualHost *:80>
    ServerAdmin ADMIN
    ServerName NOMBRE-SUBDOMINIO
    ProxyPreserveHost On
    ProxyPass / http://127.0.0.1:_PUERTO_/
    ProxyPassReverse / http://127.0.0.1:_PUERTO_/
</VirtualHost>
```

### 3. Copiar configuración a sites-enabled

`sudo cp /etc/apache2/sites-available/mi-sub-dominio.conf /etc/apache2/sites-enabled/mi-sub-dominio.conf`

### 4. Reiniciar apache 

`sudo systemctl restart apache2`
