const puppeteer = require('puppeteer');
const fs = require('fs');

exports.buscarInformacion = async function buscarInformacion(paramRut, paramDv) {
    try {
        const credenciales = {
            rut: '16527614',
            dv: '0',
            clave: 'RAUL14',
            get rutCompleto(){ return this.rut + '' + this.dv; } 
        };
        
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.goto('https://zeus.sii.cl/AUT2000/InicioAutenticacion/IngresoRutClave.html');
        
        await page.waitForSelector('form#myform');
        
        await page.evaluate((rut, clave) => {
            document.querySelector('input#rutcntr').value = rut;
            document.querySelector('input#clave').value = clave;
            document.querySelector('input[name=referencia]').value = 'https://misii.sii.cl/cgi_misii/siihome.cgi';
        }, credenciales.rutCompleto, credenciales.clave);
        
        await Promise.all([  
            page.waitForNavigation(),
            page.click('[title=Ingresar]')
        ]);
        // TODO: responder y terminar si no se pudo ingresar

        await page.goto('https://www1.sii.cl/cgi-bin/Portal001/mipeSelEmpresa.cgi');

        await page.waitFor('form#fPrmEmpPOP');

        await Promise.all([  
            page.waitForNavigation(),
            page.click('form#fPrmEmpPOP button[type=submit]')
        ]);

        await page.goto('https://www1.sii.cl/cgi-bin/Portal001/mipeGenFacEx.cgi?PTDC_CODIGO=34');
        
        await page.waitFor('#collapseRECEPTOR');

        await page.evaluate((rut, dv) => {
            document.querySelector('input#EFXP_RUT_RECEP'). value = rut;
            document.querySelector('input#EFXP_DV_RECEP'). value = dv;
        }, paramRut, paramDv);

        await Promise.all([
            page.waitForNavigation(),
            page.evaluate(() => {
                document.getElementById('EFXP_DV_RECEP').onchange();
            })
        ]);

        const respuesta = await page.evaluate(() => {
            let dataRespuesta=[];
            let obj = {};
            
            let rutInput = document.getElementsByName('EFXP_RUT_RECEP');
            let rutDvInput = document.getElementsByName('EFXP_DV_RECEP');
            let rutCompleto = rutInput[0].value + "-" + rutDvInput[0].value;
            obj = {nombre:'rut',valor:rutCompleto}
            dataRespuesta.push(obj);
            
            let direccion = document.querySelectorAll('select[name="EFXP_DIR_RECEP"] option');
            obj = {nombre:'direccion',valor: [...direccion].map(opt => opt.value)}
            dataRespuesta.push(obj);
            
            let giro = document.getElementsByName('EFXP_TIPOCOMPRA_SELECT');
            obj = {nombre:'tipoCompra',valor:giro[0].value}
            dataRespuesta.push(obj);
            
            let razonSocial = document.getElementsByName('EFXP_RZN_SOC_RECEP');
            obj = {nombre:'razonSocial',valor:razonSocial[0].value}
            dataRespuesta.push(obj);
            
            let comuna = document.getElementsByName('EFXP_CMNA_RECEP');
            obj = {nombre:'comuna',valor:comuna[0].value}
            dataRespuesta.push(obj);
            
            let ciudad = document.getElementsByName('EFXP_CIUDAD_RECEP');
            obj = {nombre:'ciudad',valor:ciudad[0].value}
            dataRespuesta.push(obj);
            
            let giroEmpresa = document.querySelectorAll('select[name="EFXP_GIRO_RECEP"] option');
            obj = {nombre:'giroEmpresa', valor: [...giroEmpresa].map(opt => opt.value)}
            dataRespuesta.push(obj);
            
            return dataRespuesta;
        });
        // Asíncrono a proposito. 
        // El receptor no tiene por que esperar a que se cierre el navegador.
        page.close().then(_ => browser.close());

        return respuesta;
        
    } catch(error) {
        fs.writeFile("last_error", error, e => {
            if (e) {
                console.log(e);
            }
        }); 
        console.error('Error!');
        console.error(error);
    }
}