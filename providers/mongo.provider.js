module.exports = (() => {
    const mongoose = require('mongoose');
    const provider = {};
    
    /**
     * @returns {mongoose.Connection} objeto db
     */
    provider.conectar = async () => {
        const nombreBd = process.env.NODE_ENV == 'produccion' ? 
            'Produccion' : 'Desarrollo';
            
        const rutaMongo = `mongodb://localhost/${nombreBd}`

        try{            
            const db = mongoose.connection;
            // TODO: archivo log en {root}/logs/mongo
            db.on('error', e => console.error('MongoError:', e));
            db.once('close', _ => console.log('Conexión a mongodb cerrada'));
            
            process.once('SIGINT', _ => {  
                process.addListener('beforeExit', _ => {
                    if (db && db.close) { db.close() }
                });
            }); 

            return await new Promise((resolve, reject) => {
                db.once('open', _ => {
                    console.log('Conexión a mongodb abierta');
                    resolve(db);
                });

                mongoose.connect(rutaMongo, { useNewUrlParser: true });
            });
        } catch (exception) {
            throw exception;
        }
    }

    return provider;
})();