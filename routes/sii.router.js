const scrapper = require('../controllers/scrapper.controller');
const mongoProvider = require('../providers/mongo.provider');

const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { title: 'Sección de herramientas referentes al sii' });
});

router.get('/info-rut', async (req, res) => {
    try {
        const rut = req.query.rut;
        const dv = req.query.dv;

        const db = mongoProvider.conectar();
        
        let contribuyente = db.collection('contribuyentes').find({ rut, dv })[0];

        if (!contribuyente) {
            contribuyente = await scrapper.buscarInformacion(rut, dv);
        }
    
        res.json(contribuyente);

    } catch(e) {
        console.log('Error');
        console.log(e);
        res.status(500).send(JSON.stringify({ error: e }));
    }
});

module.exports = router